import java.util.Scanner;

public class GetInput {
    Scanner sc = new Scanner(System.in);
    String sentence;

    public String getInput() {
        System.out.println("Please enter your sentence");
        sentence = sc.nextLine();
        return sentence;
    }
}
