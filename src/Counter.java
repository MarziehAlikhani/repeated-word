import java.util.*;

public class Counter {

    public void splitter(String input) {

        ArrayList<String> splitterArray = new ArrayList<>();
        String[] splited = input.split("\\s+");
        for (String s : splited) {
            splitterArray.add(s);
        }
        countWords(splitterArray);
    }

    public void countWords(ArrayList<String> words) {

        HashMap<String, Integer> wordMap = new HashMap<>();

        for (String word : words) {
            String tempWord = word.toLowerCase();

            if (wordMap.containsKey(tempWord)) {
                wordMap.put(tempWord, wordMap.get(tempWord) + 1);
            } else {
                wordMap.put(tempWord, 1);
            }
        }

        System.out.println("=====Repeated Words=====");
        sortOutput(wordMap);
    }

    public void sortOutput(HashMap<String,Integer> output){
        List<Map.Entry<String, Integer>> sortedList = new ArrayList<>(output.entrySet());
        sortedList.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));
        sortedList.forEach(System.out::println);
    }
}
